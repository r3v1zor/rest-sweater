package com.letscode.restsweater.domain;

public final class Views {
    public interface Id {}

    public interface IdName extends Id {}

    public interface FullMessage extends IdName{}
}
