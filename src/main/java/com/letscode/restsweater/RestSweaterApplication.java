package com.letscode.restsweater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestSweaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestSweaterApplication.class, args);
    }

}
