package com.letscode.restsweater.repo;

import com.letscode.restsweater.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepo extends JpaRepository<User, String> {

}
